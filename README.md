# Repo - Online articles

A grabbag of various scripts and data files for my online articles

## Getting Started

Each set of datafiles and scripts will be organised in their own subdirectories, with their own README file or text note (unless I've forgotten :( )).

### Prerequisites

Python (and patience for any of my mistakes or omissions) 

## Article list:

### Interactive maps with Python, Pandas and Plotly
* Directory: mapping_blogs
* Link: https://towardsdatascience.com/interactive-maps-with-python-pandas-and-plotly-following-bloggers-through-sydney-c24d6f30867e
### Basketball analytics & visualization with Python & Plotly
* Directory: basketball_lots
* Link: TBC

